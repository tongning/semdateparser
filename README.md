# Semantic Time Parser

Semantic Time Parser is a Python script that allows you to parse natural language utterances related to dates and times. It supports various date and time formats, handles common holidays in the United States, and can correct spelling errors in the input.

Features

* Parse natural language utterances related to dates and times
* Handle common U.S. holidays
* Correct spelling errors in the input text
* Extract date and time expressions from the input text
* Preprocess the input text by replacing specific phrases and holidays

Requirements
* Python 3
* Spacy
* Parsedatetime
* PySpellChecker


### Setup and usage
Install the required Python packages:
```
pip install spacy parsedatetime python-dateutil pyspellchecker
```
Download the Spacy English model:
```
python -m spacy download en_core_web_sm
```

Usage

```
python dateparse.py "Utterance to parse" "Initial date in YYYY-MM-DD or YYYY-MM-DD HH:MM format"

```

### Examples:
```
$ python dateparse.py "I went to the park 2 hours before noon today" "2023-04-17"
2023-04-17 10:00:00

$ python dateparse.py "Teuesdey oif Nexx weak att 9 pm" "2023-02-24"
2023-02-28 21:00:00

$ python dateparse.py "next tuesday at 9pm" "2023-02-24"
2023-02-28 21:00:00

$ python dateparse.py "tomorrow at 11am" "2023-02-24"
2023-02-25 11:00:00

$ python dateparse.py "on christmas at 2 in the afternoon" "2023-02-24"
2023-12-25 14:00:00

$ python dateparse.py "2 hours from now" "2023-02-24 03:30"
2023-02-24 05:30:00
```

### How it works:
This script is built on several open-source libraries. The pyspellchecker library is used to fix spelling errors in the input sentence, and
the spaCy NLP library is used to classify words to identify the parts of the sentence that contain temporal information. The `parseDateTime`
library is used to process the cleaned text into a result `datetime`.


### Testing:
Run unit tests using pytest:
```
$ pytest test_dateparse.py
```