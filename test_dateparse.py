import pytest
from datetime import datetime
from dateparse import SemanticTimeParser, parse_initial_date


@pytest.fixture
def parser():
    return SemanticTimeParser()

@pytest.mark.parametrize(
    "utterance, initial_date, expected",
    [
        ("This coming Tuesday at 8am", "2023-04-17", datetime(2023, 4, 18, 8)),
        ("3 days after next Wednesday, at 8pm", "2023-04-17", datetime(2023, 4, 29, 20)),
        ("Thanksgiving at 5pm", "2023-11-01", datetime(2023, 11, 23, 17, 00)),
        ("Thursday 3 weeks from now at 2pm", "2023-04-17", datetime(2023, 5, 8, 14, 00)),
        ("5 hours before noon today", "2023-04-17", datetime(2023, 4, 17, 7, 00)),
    ],
)
def test_perform_query(parser, utterance, initial_date, expected):
    initial_date = parse_initial_date(initial_date)
    parsed_date = parser.perform_query(utterance, initial_date)
    assert parsed_date == expected
