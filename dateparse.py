import argparse
import datetime
import re
import parsedatetime as pdt
from enum import Enum
from dateutil.relativedelta import relativedelta, MO, TH
import math
import spacy
from spellchecker import SpellChecker

class Holiday(Enum):
    """
    Enum class for US federal holidays
    """
    NEW_YEAR = 'new_year'
    MLK_DAY = 'mlk_day'
    PRESIDENTS_DAY = 'presidents_day'
    MEMORIAL_DAY = 'memorial_day'
    JUNETEENTH = 'juneteenth'
    INDEPENDENCE_DAY = 'independence_day'
    LABOR_DAY = 'labor_day'
    COLUMBUS_DAY = 'columbus_day'
    VETERANS_DAY = 'veterans_day'
    THANKSGIVING_DAY = 'thanksgiving_day'
    CHRISTMAS = 'christmas_day'

class SemanticTimeParser():
    def __init__(self):
        self.cal = pdt.Calendar()
        self.nlp = spacy.load("en_core_web_sm") 


    def correct_spelling(self, text):
        """
        Correct spelling errors in the input text. Corrections are only applied if the word contains alpha characters
        and the output of the correction is not empty, to avoid potentially removing important date information.
        """
        spell = SpellChecker()
        words = text.split()
        corrected_words = [spell.correction(word) if self.should_correct_word(word) else word for word in words]

        return ' '.join(corrected_words)

    def should_correct_word(self, word):
        spell = SpellChecker()
        return word.isalpha() and spell.correction(word) is not None and word not in ["am", "pm"]

    def extract_date_time_expression(self, text):
        """
        Use spacy Named Entity Recognition (NER) to extract the region of the text that contains date and time information.
        For example, given text 'I want to go to the park 5 hours after 2pm today, and walk my dog' it will extract
        '5 hours after 2pm'. This way, we can focus on the most important part of the utterance.

        This method works by finding tokens classified as date, time, or ordinal, as well as tokens that modify a date, time, or ordinal
        such as 'next' or 'before'. It extracts the region of the string between the earliest and latest relevant token.
        """
        doc = self.nlp(text)
        earliest_start = math.inf
        latest_end = -math.inf
        for token in doc:
            if token.ent_type_ in ("DATE", "TIME", "ORDINAL"):
                start_index = token.idx
                end_index = start_index + len(token.text)
                if start_index < earliest_start:
                    earliest_start = start_index
                if end_index > latest_end:
                    latest_end = end_index
                for child in token.children:
                    if child.dep_ == "amod":
                        child_start = child.idx
                        child_end = child_start + len(child.text)
                        if child_start < earliest_start:
                            earliest_start = child_start
                        if child_end > latest_end:
                            latest_end = child_end
        return text[earliest_start:latest_end]



    def preprocess_utterance(self, utterance, initial_date):
        """
        This function performs ad-hoc text replacements to improve parsing performance. It can perform simple text replacements
        such as "in the morning"->"am", or replacing the names of holidays.
        """
        utterance = re.sub(r'(\d+)\s+(in the morning|the morning|this morning)', r'\1am', utterance)
        utterance = re.sub(r'(\d+)\s+(in the afternoon|the afternoon|this afternoon)', r'\1pm', utterance)
        utterance = re.sub(r'(\d+)\s+(in the evening|the evening|this evening)', r'\1pm', utterance)
        utterance = re.sub(r'(new year(?:\'s)?)', self.get_holiday_date(Holiday.NEW_YEAR, initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(mlk day|martin luther king day)', self.get_holiday_date(Holiday.MLK_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(presidents day)', self.get_holiday_date(Holiday.PRESIDENTS_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(memorial day)', self.get_holiday_date(Holiday.MEMORIAL_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(juneteenth)', self.get_holiday_date(Holiday.JUNETEENTH,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(independence day)', self.get_holiday_date(Holiday.INDEPENDENCE_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(labor day)', self.get_holiday_date(Holiday.LABOR_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(columbus day)', self.get_holiday_date(Holiday.COLUMBUS_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(veterans day)', self.get_holiday_date(Holiday.VETERANS_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(thanksgiving)', self.get_holiday_date(Holiday.THANKSGIVING_DAY,initial_date.year), utterance, flags=re.IGNORECASE)
        utterance = re.sub(r'(christmas)', self.get_holiday_date(Holiday.CHRISTMAS,initial_date.year), utterance, flags=re.IGNORECASE)
        return utterance

    def get_holiday_date(self, holiday, year):
        """
        Translates holidays to the correct date. Currently supports US federal holidays.
        """
        if holiday == Holiday.NEW_YEAR:
            date = datetime.date(year, 1, 1)
        elif holiday == Holiday.MLK_DAY:
            date = datetime.date(year, 1, 1) + relativedelta(weekday=MO(+3))
        elif holiday == Holiday.PRESIDENTS_DAY:
            date = datetime.date(year, 2, 1) + relativedelta(weekday=MO(+3))
        elif holiday == Holiday.MEMORIAL_DAY:
            date = datetime.date(year, 5, 31) + relativedelta(weekday=MO(-1))
        elif holiday == Holiday.JUNETEENTH:
            date = datetime.date(year, 6, 19)
        elif holiday == Holiday.INDEPENDENCE_DAY:
            date = datetime.date(year, 7, 4)
        elif holiday == Holiday.LABOR_DAY:
            date = datetime.date(year, 9, 1) + relativedelta(weekday=MO(+1))
        elif holiday == Holiday.COLUMBUS_DAY:
            date = datetime.date(year, 10, 1) + relativedelta(weekday=MO(+2))
        elif holiday == Holiday.VETERANS_DAY:
            date = datetime.date(year, 11, 11)
        elif holiday == Holiday.THANKSGIVING_DAY:
            date = datetime.date(year, 11, 1) + relativedelta(weekday=TH(+4))
        elif holiday == Holiday.CHRISTMAS:
            date = datetime.date(year, 12, 25)

        return date.strftime('%b %-d')


    def parse_date_string(self, utterance, initial_date):
        """
        Preprocess the input utterance, then run through parsedatetime library
        """
        utterance = self.correct_spelling(utterance)
        utterance = self.extract_date_time_expression(utterance)
        utterance = self.preprocess_utterance(utterance, initial_date)

        # Parse the date string
        parsed_date, result_code = self.cal.parse(utterance, initial_date)

        # Check if parsing was successful
        if result_code != 0:
            return datetime.datetime(*parsed_date[:6])
        else:
            return None

    def perform_query(self, utterance, initial_date):
        return self.parse_date_string(utterance, initial_date)

def parse_initial_date(date_string):
    try:
        return datetime.datetime.strptime(date_string, '%Y-%m-%d %H:%M')
    except ValueError:
        return datetime.datetime.strptime(date_string, '%Y-%m-%d')

def main(args):
    parser = SemanticTimeParser()
    parsed_date = parser.perform_query(args.utterance, args.initial_date)
    print(parsed_date)

if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(description="Parse utterances related to temporal times")
    arg_parser.add_argument("utterance", help="Utterance to parse (e.g., 'This coming Tuesday at 8am')")
    arg_parser.add_argument("initial_date", help="Initial date in YYYY-MM-DD or YYYY-MM-DD HH:MM format (e.g., '2023-04-17')", type=parse_initial_date)
    args = arg_parser.parse_args()
    main(args)
